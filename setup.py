#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2018, CERN
# This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".
# In applying this license, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as Intergovernmental Organization
# or submit itself to any jurisdiction.


import os

from setuptools import find_packages, setup

readme = open('README.md').read()
history = open('CHANGES.md').read()

tests_require = [
    'flake8>=3.7.9',
    'isort==4.3.21',
    'pytest-mock>=1.6.0'
]

install_requires = [
    'backoff>=1.8.0,<1.9.0',
    'beautifulsoup4==4.7.1',
    'import_string>=0.1.0,<0.2.0',
    'redis>=3.2.1,<3.3.0',
    'requests>=2.19.1,<2.20.0',
    'Scrapy>=1.6.0,<1.7.0',
    'tika>=1.18,<1.19',
    'w3lib>=1.19.0,<1.20.0',
    'sentry-sdk==0.10.2',
    # Temporary fix: https://github.com/scrapy/scrapy/pull/5401
    'Twisted<22.1.0'
]


extras_require = {'tests': tests_require, 'all': []}
for reqs in extras_require.values():
    extras_require['all'].extend(reqs)

packages = find_packages()

# Get the version string. Cannot be done with import!
g = {}
with open(os.path.join("cern_web_crawler", "version.py"), "rt") as fp:
    exec(fp.read(), g)
    version = g["__version__"]

setup(
    name='cern_web_crawler',
    version=version,
    description='CERN Web Crawler',
    long_description=readme + '\n\n' + history,
    license='GPLv3',
    author='CERN',
    author_email='cernsearch.support@cern.ch',
    url='http://search.cern.ch/',
    packages=packages,
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    extras_require=extras_require,
    install_requires=install_requires,
    tests_require=tests_require,
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Development Status :: 1 - Pre-Alpha',
    ],
)
