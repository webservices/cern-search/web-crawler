
###################  Docker development helpful directives  ####################
#
# Usage:
# make logs                   # displays log outputs from running services
# make build-env              # build environment, create and start containers
# make env                    # build environment, load fixtures and enters shell
# make destroy-env            # stop and remove containers, networks, images, and volume
# make reload-env             # restart containers, networks, images, and volume
# make shell-env              # start bash inside service
# make run                    # run crawler inside service
# make test                   # runs tests
# make lint                   # runs linting tools

SERVICE_NAME :=  cern-crawler
DOCKER_FILE := docker-compose.yml
MODE?=test
TEST_MODE=test

ifeq ($(MODE),$(TEST_MODE))
DOCKER_FILE := docker-compose.test.yml
else
DOCKER_FILE := docker-compose.yml
endif

build-env:
	docker-compose -f $(DOCKER_FILE) up -d --remove-orphans
.PHONY: build-env

load-fixtures:
	@echo "https://scrapy.org/" > sites.txt
.PHONY: load-fixtures

logs:
	docker-compose -f $(DOCKER_FILE) logs -f
.PHONY: logs

destroy-env:
	docker-compose -f $(DOCKER_FILE) down --volumes
	docker-compose -f $(DOCKER_FILE) rm -f
.PHONY: destroy-env

stop-env:
	docker-compose -f $(DOCKER_FILE) down --volumes
.PHONY: stop-env

reload-env: destroy-env env
.PHONY: reload-env

shell-env:
	docker-compose -f $(DOCKER_FILE) exec $(SERVICE_NAME) /bin/bash
.PHONY: shell-env

flush-redis:
	docker-compose -f $(DOCKER_FILE) exec redis /bin/bash -c "redis-cli flushall"
.PHONY: flush-redis

env: load-fixtures build-env shell-env
.PHONY: env

run:
	docker-compose -f $(DOCKER_FILE) exec -T $(SERVICE_NAME) /bin/bash -c \
	"python cern_web_crawler/manager.py"
.PHONY: run

pytest:
	docker-compose -f $(DOCKER_FILE) exec -T $(SERVICE_NAME) /bin/bash -c \
	"pytest tests -vv;"
.PHONY: pytest

test: stop-env build-env pytest
.PHONY: test

lint:
	docker-compose -f $(DOCKER_FILE) exec -T $(SERVICE_NAME) /bin/bash -c \
		"echo running isort...; \
		isort -rc -c -df; \
		echo running flake8...; \
		flake8 --max-complexity 10 --ignore E501,D401"
.PHONY: lint
