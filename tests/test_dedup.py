#!/usr/bin/python
# Copyright (C) 2018, CERN
# This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".
# In applying this license, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as Intergovernmental Organization
# or submit itself to any jurisdiction.

from __future__ import print_function
from sys import getsizeof, stderr
from itertools import chain
from collections import deque

import time

try:
    from reprlib import repr
except ImportError:
    pass


import random
import string

"""
Code from GitHub ActiveState/code. Compute and Memory footprint section, recipe number 577504.
"""

def total_size(o, handlers={}, verbose=False):
    """ Returns the approximate memory footprint an object and all of its contents.

    Automatically finds the contents of the following builtin containers and
    their subclasses:  tuple, list, deque, dict, set and frozenset.
    To search other containers, add handlers to iterate over their contents:

        handlers = {SomeContainerClass: iter,
                    OtherContainerClass: OtherContainerClass.get_elements}

    """
    dict_handler = lambda d: chain.from_iterable(d.items())
    all_handlers = {tuple: iter,
                    list: iter,
                    deque: iter,
                    dict: dict_handler,
                    set: iter,
                    frozenset: iter,
                    }
    all_handlers.update(handlers)  # user handlers take precedence
    seen = set()  # track which object id's have already been seen
    default_size = getsizeof(0)  # estimate sizeof object without __sizeof__

    def sizeof(o):
        if id(o) in seen:  # do not double count the same object
            return 0
        seen.add(id(o))
        s = getsizeof(o, default_size)

        if verbose:
            print(s, type(o), repr(o), file=stderr)

        for typ, handler in all_handlers.items():
            if isinstance(o, typ):
                s += sum(map(sizeof, handler(o)))
                break
        return s

    return sizeof(o)


def random_word(length):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))


if __name__ == '__main__':

    sample_size = 16000

    # Generate list
    items = []
    counter = 0
    while counter < sample_size:
        items.append(random_word(60))
        counter += 1

    ######## ARRAY ########
    # Test insertions and 2000 dup checks
    dedup = [items[0]]  # Filter structure, initialized to avoid 0 % 8 case
    counter = 1
    start_time = time.time()
    dedup_check_fail = 0
    dedup_found = 0
    while counter < sample_size:
        if counter % 8 == 0:
            if items[random.randint(0, len(dedup)-1)] not in dedup:
                dedup_check_fail += 1
        if items[counter] not in dedup:
            dedup.append(items[counter])
        else:
            dedup_found +=1
        counter += 1
    # your code
    elapsed_time = time.time() - start_time
    print('-------------------- ARRAY --------------------')
    print(f'Total size of array dedup filter is {total_size(dedup)} bytes')
    print(f'Total elapsed time {elapsed_time}')
    print(f'Duplicate checks failed {dedup_check_fail}')
    print(f'Duplicates found in array {dedup_found}')
    print('-----------------------------------------------')

    ######## SET ########
    dedup = set()
    dedup.add(items[0])  # Filter structure, initialized to avoid 0 % 8 case
    counter = 1
    start_time = time.time()
    dedup_check_fail = 0
    dedup_found = 0
    while counter < sample_size:
        if counter % 8 == 0:
            if items[random.randint(0, len(dedup)-1)] not in dedup:
                dedup_check_fail += 1
        if items[counter] not in dedup:
            dedup.add(items[counter])
        else:
            dedup_found +=1
        counter += 1
    # your code
    elapsed_time = time.time() - start_time
    print('--------------------  SET  --------------------')
    print('Total size of set dedup filter is {size} bytes'.format(size=total_size(dedup)))
    print('Total elapsed time {t}'.format(t=elapsed_time))
    print('Duplicate checks failed {num_failures}'.format(num_failures=dedup_check_fail))
    print('Duplicates found in set {found}'.format(found=dedup_found))
    print('-----------------------------------------------')
