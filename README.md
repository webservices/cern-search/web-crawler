# CERN Search Crawler

Web Services hosting provides different infrastructures (IIS, Drupal, OpenShift, etc.) therefore a generic method to obtain their content is needed.

The CERN Search Crawler is a focused crawler eveloped using [scrapy](https://doc.scrapy.org/en/latest/) for the ``cern.ch`` websphere. The goal of this crawler is to feed te CERN Search Engine with the content corresponding to all personal/project websites hosted at CERN.


## Configuration ([Source File](https://gitlab.cern.ch/webservices/cern-search/web-crawler/blob/master/web_crawler/settings.py))

First of all in order to identify the spider the ``USE-AGENT`` has been set to ``CERN Search (+https://search.cern.ch)``.

The crawler needs to respect web sites owners' will for privacy, therefore the ``ROBOTSTXT_OBEY`` setting has been set to ``TRUE``. Meaning, that the crawler wil respect the ``robots.txt`` file.

Since the goal is just to retrieve the websites' content in order to provide relevant resuls on the CERN Search site, and the amount of links in sites can make the crawler processing time grow exponentaly, the ``DEPTH_LIMIT`` has been set to ``3``. Infinite loops / spider traps are tackled with the use of the "DEDUP" (Deduplicate) pipeline explained further in this document.

``DNSCACHE_ENABLED`` is set to ``False`` since all the requests are for websites hosted at CERN the local DNS servers responde fast enough (Therefore the timeout is also set to a low value, ``20`` seconds). Almost no difference was appreciated during the tests, by disabling the cache we make sure that no errors due to a site being moved in between requests occur.

Finally, the ``AUTOTHROTTLE_ENABLE`` has bee set to ``False``, which means the crawler is not polite. In consequnce, it will not wait to perform request that target the same server. This decission was taken based on two reasons:

- The load that the crawler generates to the web servers is negligible compared to the daily load.
- The amount of websites hosted is bigger (in the order of 100 to 1) than the servers that serve those sites. Therefore, if politeness is applied the crawler would take a non-admisible amout of time to scrape all the sites.

## Spiders

This crawler makes use of two spiders one for [public sites](https://gitlab.cern.ch/webservices/cern-search/web-crawler/blob/master/web_crawler/spiders/public.py) and another for private ones (**WORK IN PROGRESS**).

### Web Service Public

The crawler obtains the starting sites from a [data loading](https://gitlab.cern.ch/webservices/cern-search/web-crawler/blob/master/web_crawler/data_loader.py) function, which can be specified in each environment.

The ``Content-Type`` is checked in order to see if the link corresponds to a binary file or a website. In the first case, files are processed by [Tika](https://tika.apache.org/). In the later case the URL, the title and the parsed content of it are obtained (e.g. JavaScript code is removed).

Here, the concept of "origin" URL plays its role. A URL is considered to be the "origin" when it has not been reached through another site (i.e. the depth level is greater or equal to 1). This distinction is done by using an extra field ``origin`` in the content. When the ``origin`` and ``url`` fields value match, it means that the content belongs to an origin site.

The content of non-origin sites will be added as "extras" to the content of its origin (this task is performed by the indexer). However, each non-origin site will also be sent to the indexer as an origin. This happens because we want all sites and subsites found to be searched separatelly, but adding followed links content to the origin helps to improve the relevance on search operations.

#### Middlewares

- [**SizeDownloaderMiddleware**](https://gitlab.cern.ch/webservices/cern-search/web-crawler/blob/master/web_crawler/middlewares.py) (Downloader Middleware): Performs a ``HEAD`` request to check the ``Content-length`` of the website or file, in order to discard those that are larger than ``PUBLIC_CRAWLER_MAX_SITE_SIZE`` (By default 2GB). This is done to avoid some files containing large amount of multimedia content with no valuable text, no case of a website weighting more that limit has been observed. As a side effect, those sites that return errors are discarded.

#### Pipelines

- [**DedupFilter**]((https://gitlab.cern.ch/webservices/cern-search/web-crawler/blob/master/web_crawler/pipelines.py): It checks that the url, coming from the same origin (meaning that a site might be crawled multiple times if coming form different origins), has not been already visited. For this purpouse a ``set`` is used, with "url::origin" as value. 

- [**IndexerQueuePipeline**]((https://gitlab.cern.ch/webservices/cern-search/web-crawler/blob/master/web_crawler/pipelines.py): Sends the items (website content and information) of the successfully crawled sites to an ActiveMQ queue. The Indexer component will consume and request the corresponding CERN Search REST API to index the item.

### Web Service Private

This spider extracts the content of a site in the same way than the "Public" sites spider. However, it has to bypass the [Single-Sign On](https://en.wikipedia.org/wiki/Single_sign-on) portal set up at [CERN](https://cern.service-now.com/service-portal/article.do?n=KB0004765).

In addition it needs to extract the Access Rights from the database/.htaccess files in order to apply it to the content when it is searched on CERN Search.

## Configuration

**NOTE**: All the following settings can be modified as environmental variables, but the prefix ``WCRAWLER`` has to be added at the beggining of each one.

### Crawler

- DEPTH_LIMIT: Limit the depth of the followed links. By default set to ``1``.

### Messaging

- BROKER_HOSTNAME: Hostname of messaging broker. By default set to ``localhost``.
- BROKER_LOADER_FUNCTION: Name of the function to be used to obtain one of the broker's IP address. By default set to ``cern_web_crawler.messaging:get_broker``.
- BROKER_LIST_LOADER_FUNCTION: Name of the function to be used to obtain the all the broker's IP addresses. By default set to ``cern_web_crawler.messaging:get_broker_list``.
- BROKER_PORT: Port of the messaging broker. By default set to ``61613``.
- BROKER_SSL_CERT: Path/to/filename of the SSL certificate to be used to authenticate the crawler against the messaging service. No default value set, **required**.
- MESSAGING_QUEUE: Name of the queue to which to send the messages. By default set to ``/queue/test``.
- SYSTEM_NAME: Name of the system to be able to be identified by the indexer. By default set to ``crawler``.

### Spiders

#### Public

- PUBLIC_CRAWLER_TEXT_CONTENT_TYPES: ``Set`` of content types identified to belong to text content. by default set to ``{'text/plain', 'text/html', 'application/json', 'application/rtf', 'application/xhtml+xml', 'application/xml'}``.
- PUBLIC_CRAWLER_MAX_SITE_SIZE: Maximum size of a site/file in order to be processed in **bytes**. Sites/files weighting more than this value will be ignored. By default set to ``1073741824``Bytes.
- PUBLIC_CRAWLER_AVOID_LINKS: List of link types to be avoided, not belonging to URLs. By default set to ``['mailto', 'gtalk', 'callto']``.
- PUBLIC_CRAWLER_ENCODING: Encoding to be used by the crawler. By default set to ``ISO-8859-1``.
- PUBLIC_SITES_FUNCTION: Name of the function to be used to obtain the starting list of sites to crawl. By default set to ``cern_web_crawler.data_loader:import_sites_from_file``.
- PUBLIC_SITES_FILENAME: Path/to/filename of the file in which the starting sites are located. By default set to ``sites.txt``. It is only required when ``PUBLIC_SITES_FUNCTION`` is set to its default value.
- PUBLIC_SITES_REST_ENDPOINT: Endpoint to query for the list of starting sites. By default set to ``http://localhost/sites``. It is only required when ``PUBLIC_SITES_FUNCTION`` is set to ``cern_web_crawler.data_loader:import_sites_from_rest``.
- PUBLIC_SITES_REST_CREDENTIALS: Credentials to authenticate the crawler against the before mentioned RESTful API. No default value, but it is only required when ``PUBLIC_SITES_FUNCTION`` is set to ``cern_web_crawler.data_loader:import_sites_from_rest``.
- PUBLIC_CRAWLER_CERNSEARCH_INSTANCE: CERN Search instance in which the results of the crawler should be indexed. By default set to ``cernsearch-test``.
- PUBLIC_CRAWLER_LOGLEVEL: Logging level of the crawler. By default set to ``DEBUG``.
- PUBLIC_CRAWLER_SENTRY_DSN: DSN of the Sentry project to which to send the logs. No default value set, **required**.

## Deployment

Deployment is done automatically in Openshift using Gitlab-CI pipelines. Every tag/push to ``master`` will be deployed in Openshift prod, and changes in the ``dev`` in Openshift-dev.

### Docker

The dockerfile is design to execute the spider ``CERN Search public crawler`` with the default settings (Database, Pipelines, etc.). Therfore, add the corresponding ENV variables.

Note: The command (``CMD``) can be modify with the ``command`` attribute in OpenShift in order to run all spiders.

## Logging

Logging information is sent to a Sentry instance in order to preserve the logs after the OpenShift pods have been killed/terminated. In addition Sentry offers breadcrumbs and context on each message, greatly helping debugging tasks. 