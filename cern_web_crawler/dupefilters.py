#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  Copyright (C) 2018, CERN
#
#  This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".
# In applying this license, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as Intergovernmental Organization
# or submit itself to any jurisdiction.

import hashlib
import os

from scrapy.dupefilters import RFPDupeFilter


class WebservicesDupeFilter(RFPDupeFilter):

    def request_seen(self, request):
        fp = hashlib.sha224(
            f'{request.url}::{request.meta.get("origin")}'.encode('utf-8')
        ).hexdigest()

        if fp in self.fingerprints:
            return True
        self.fingerprints.add(fp)
        if self.file:
            self.file.write(fp + os.linesep)
