#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018, CERN
#
# This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".
# In applying this license, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as Intergovernmental Organization
# or submit itself to any jurisdiction.

"""
###################################
#### Scrapy / Crawler settings ####
###################################
# Scrapy settings for cern_web_crawler project
#
# For simplicity, this file contains only settings considered important for
# the project. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html
"""
import ast
import os

from scrapy.settings import Settings

from cern_web_crawler import utils
from cern_web_crawler.spiders.public import CERNPublic

# Follow just links coming from the zero-level website
DEPTH_LIMIT = utils.evaluate_config_param(os.getenv('WCRAWLER_DEPTH_LIMIT'))
if not DEPTH_LIMIT:
    DEPTH_LIMIT = 1

BOT_NAME = 'cern_web_crawler'

SPIDER_MODULES = ['cern_web_crawler.spiders']
NEWSPIDER_MODULE = 'cern_web_crawler.spiders'

###################################
####  MANAGER / REDIS Setting  ####
###################################

REDIS_HOST = os.getenv('WCRAWLER_REDIS_HOST', 'localhost')
REDIS_PORT = os.getenv('WCRAWLER_REDIS_PORT', 6379)
REDIS_DB = os.getenv('WCRAWLER_REDIS_DB', 0)

###################################
#### Public / Crawler settings ####
###################################

# CERN Search REST API
PUBLIC_S_API_ENDPOINT = os.getenv('WCRAWLER_PUBLIC_SPIDER_API_ENDPOINT',
                                  'https://webservices-search-dev.web.cern.ch')
PUBLIC_S_API_JSONSCHEMAS_HOST = os.getenv('WCRAWLER_PUBLIC_SPIDER_API_JSONSCHEMAS_HOST',
                                  'https://webservices-search-dev.web.cern.ch')
PUBLIC_S_API_TOKEN = os.getenv('WCRAWLER_PUBLIC_SPIDER_API_TOKEN')
# BACKOFF
PUBLIC_S_API_DELAY = os.getenv('WCRAWLER_PUBLIC_API_DELAY', 1)
PUBLIC_S_API_MAX_DELAY = os.getenv('WCRAWLER_PUBLIC_API_MAX_DELAY', 64)
PUBLIC_S_API_MAX_RETRIES = os.getenv('WCRAWLER_PUBLIC_API_MAX_RETRIES', 3)
PUBLIC_CRAWLER_TEXT_CONTENT_TYPES = utils.evaluate_config_param(
    os.getenv('WCRAWLER_PUBLIC_CRAWLER_TEXT_CONTENT_TYPES')
)
# Taken from https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types
if not PUBLIC_CRAWLER_TEXT_CONTENT_TYPES:
    PUBLIC_CRAWLER_TEXT_CONTENT_TYPES = {'text/plain',
                                         'text/html',
                                         'text/xml',
                                         'application/json',
                                         'application/rtf',
                                         'application/xhtml+xml',
                                         'application/xml'
                                         }
PUBLIC_CRAWLER_MAX_SITE_SIZE = os.getenv('WCRAWLER_PUBLIC_CRAWLER_MAX_SITE_SIZE',
                                         1073741824)  # 1GB
PUBLIC_CRAWLER_ENCODING = os.getenv('WCRAWLER_PUBLIC_CRAWLER_ENCODING',
                                    'ISO-8859-1')
# Default sites loading. Usually overwritten via ENV variables
PUBLIC_SITES_FUNCTION = os.getenv('WCRAWLER_PUBLIC_SITES_FUNCTION',
                                  'cern_web_crawler.data_loader:import_sites_from_file')
# Default file. Might not exists in the server.
PUBLIC_SITES_FILENAME = os.getenv('WCRAWLER_PUBLIC_SITES_FILENAME', 'sites.txt')
# Default endpoint. No server side logic implemented in this project
PUBLIC_SITES_REST_ENDPOINT = os.getenv('WCRAWLER_PUBLIC_SITES_REST_ENDPOINT',
                                       'http://localhost/sites')
PUBLIC_SITES_REST_CREDENTIALS = os.getenv('WCRAWLER_PUBLIC_SITES_REST_CREDENTIALS')
PUBLIC_SITES_REST_TYPE = os.getenv('WCRAWLER_PUBLIC_SITES_REST_TYPE', 'Test')
PUBLIC_CRAWLER_LOGLEVEL = os.getenv('WCRAWLER_PUBLIC_CRAWLER_LOGLEVEL', 'DEBUG')
PUBLIC_CRAWLER_SENTRY_DSN = os.getenv('WCRAWLER_PUBLIC_CRAWLER_SENTRY_DSN')
PUBLIC_CRAWLER_SENTRY_LOGLEVEL = os.getenv('WCRAWLER_PUBLIC_CRAWLER_SENTRY_LOGLEVEL', 'WARNING')
DENY_LIST = ast.literal_eval(os.getenv('WCRAWLER_DENY_LIST', 'None')) or set()
DENY_EXTENSIONS = ast.literal_eval(os.getenv('WCRAWLER_DENY_EXTENSIONS', 'None')) or set()
ALLOWED_EXTENSIONS = ast.literal_eval(os.getenv('WCRAWLER_ALLOW_EXTENSIONS', 'None')) or set()
FORCE = ast.literal_eval(os.getenv('WCRAWLER_FORCE', 'False'))
CRAWL_WEB = ast.literal_eval(os.getenv('WCRAWLER_CRAWEL_WEB', 'True'))
TELNETCONSOLE_ENABLED = ast.literal_eval(os.getenv('TELNETCONSOLE_ENABLED', 'True'))
TELNETCONSOLE_PASSWORD = os.getenv('TELNETCONSOLE_PASSWORD', 'cern-crawler')

generic_settings = {
    'BOT_NAME': 'cern-search-crawler',
    'USER_AGENT': 'CERN Search Crawler',
    'ROBOTSTXT_OBEY': True,
    'DEPTH_LIMIT': DEPTH_LIMIT,
    'DNSCACHE_ENABLED': False,
    'DNS_TIMEOUT': 20,
    'DOWNLOAD_FAIL_ON_DATALOSS': False,
    'DOWNLOAD_TIMEOUT': 180,
    'RETRY_ENABLED': True,
    'AUTOTHROTTLE_ENABLED': False,
    'DOWNLOAD_WARNSIZE': 10000000,
    'DOWNLOAD_MAXSIZE': 20000000,
    'TELNETCONSOLE_PASSWORD': TELNETCONSOLE_PASSWORD
}


def build_settings(spider_type):
    if spider_type == CERNPublic.name:
        custom_settings = {
            # 'DUPEFILTER_CLASS': 'cern_web_crawler.dupefilters.WebservicesDupeFilter',
            'ITEM_PIPELINES': {
                'cern_web_crawler.pipelines.SearchAPIPipeline': 800,
            },
            'DOWNLOADER_MIDDLEWARES': {
                'cern_web_crawler.middlewares.SizeDownloaderMiddleware': 120,
            },
            'EXTENSIONS': {
                'cern_web_crawler.extensions.SentryLogging': -1,  # Load SentryLogging extension before others
                'scrapy.extensions.telnet.TelnetConsole': 500
            },
            'API_ENDPOINT': PUBLIC_S_API_ENDPOINT,
            'JSONSCHEMAS_HOST': PUBLIC_S_API_JSONSCHEMAS_HOST,
            'API_TOKEN': PUBLIC_S_API_TOKEN,
            'API_DELAY': PUBLIC_S_API_DELAY,
            'API_MAX_DELAY': PUBLIC_S_API_MAX_DELAY,
            'API_MAX_RETRIES': PUBLIC_S_API_MAX_RETRIES,
            'TEXT_CONTENT_TYPES': PUBLIC_CRAWLER_TEXT_CONTENT_TYPES,
            'MAX_SITE_SIZE': PUBLIC_CRAWLER_MAX_SITE_SIZE,
            'ENCODING': PUBLIC_CRAWLER_ENCODING, #  TODO: Is encoding a needed attribute?
            'SITES_FUNCTION': PUBLIC_SITES_FUNCTION,
            'SITES_FILENAME': PUBLIC_SITES_FILENAME,
            'SITES_REST_ENDPOINT': PUBLIC_SITES_REST_ENDPOINT,
            'SITES_REST_CREDENTIALS': PUBLIC_SITES_REST_CREDENTIALS,
            'SITES_REST_TYPE': PUBLIC_SITES_REST_TYPE,
            'LOGLEVEL': PUBLIC_CRAWLER_LOGLEVEL,
            'SENTRY_DSN': PUBLIC_CRAWLER_SENTRY_DSN,
            'SENTRY_LOGLEVEL': PUBLIC_CRAWLER_SENTRY_LOGLEVEL

        }

        custom_settings.update(generic_settings)

        return Settings(values=custom_settings)

    return generic_settings
