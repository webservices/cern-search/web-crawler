#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2018, CERN
# This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".
# In applying this license, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as Intergovernmental Organization
# or submit itself to any jurisdiction.

import ast
from urllib.parse import quote


def evaluate_config_param(param):
    # Evaluate value
    try:
        return ast.literal_eval(param)

    except (SyntaxError, ValueError):
        return None


def clean_url(url):
    """
    All ES special characters need to be escaped
    + - = && || > < ! ( ) { } [ ] ^ " ~ * ? : \ /

    Only / and ~ have been present in URLs.
    '-' Does not make the request fail

    :param url:
    :return:
    """

    return quote(url.replace('/', '\/').replace('~', '\~'), safe='')


class TooManyRequests(Exception):
    pass
