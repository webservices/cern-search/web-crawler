#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2018, CERN
# This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".
# In applying this license, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as Intergovernmental Organization
# or submit itself to any jurisdiction.

"""
Define your item pipelines here

Don't forget to add your pipeline to the ITEM_PIPELINES setting
See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
"""

import csv
import json
import logging
from http import HTTPStatus

import backoff
import requests

from cern_web_crawler import settings
from cern_web_crawler.utils import TooManyRequests, clean_url


class CSVPipeline(object):

    def __init__(self):
        self.max_rows = 100
        self.counter = 0
        self.csvfile = open('sample.csv', 'w')
        column_names = ['name', 'url', 'website', 'analytics_relevance',
                        'origin', 'content', 'last_updated', 'attachment', 'instance', 'access']
        self.writer = csv.DictWriter(self.csvfile, column_names, delimiter='|', quotechar='"')

    def open_spider(self, spider):
        self.writer.writeheader()

    def close_spider(self, spider):
        self.csvfile.close()

    def process_item(self, item, spider):

        if self.counter < self.max_rows:
            try:
                self.writer.writerow({
                    'name': item.get('name'),
                    'url': item.get('url'),
                    'website': item.get('website'),
                    'analytics_relevance': -1,
                    'origin': item.get('origin'),
                    'content': item.get('content'),
                    'last_updated': item.get('last_updated').strftime("%Y-%m-%dZ%H:%M"),
                    'attachment': str(item.get('attachment')),
                    'instance': item.get('instance'),
                    'access': 'public'
                })
                self.counter += 1
            except UnicodeEncodeError as e:
                print(e)
        else:
            logging.info('Finished taking sample...')

        return item


def get_extra(doc_list, doc_url):
    for doc in doc_list:
        if doc['url'] == doc_url:
            return doc
    return None


class SearchAPIPipeline(object):
    def __init__(self, api_token, api_endpoint, jsonschema_host):
        self.api_endpoint = api_endpoint
        self.jsonschema_host = jsonschema_host
        self.headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": f'Bearer {api_token}'
        }

        self.attachment_headers = {
            "Accept": "application/json",
            "Content-Type": "application/octet-stream",
            "Authorization": f'Bearer {api_token}'
        }

        self.patch_headers = {
            "Accept": "application/json",
            "Content-Type": "application/json-patch+json",
            "Authorization": f'Bearer {api_token}'
        }

    @classmethod
    def from_crawler(cls, crawler):
        settings = crawler.settings
        api_token = settings.get("API_TOKEN")
        api_endpoint = settings.get("API_ENDPOINT")
        jsonschema_host = settings.get("JSONSCHEMAS_HOST")

        return cls(api_token, api_endpoint, jsonschema_host)

    def process_item(self, item, spider):
        url = item.get('url')
        logging.debug(f"Processing url {url} origin {item.get('origin')}")

        api_doc = self.get_doc(url)

        if item.get('attachment'):
            logging.debug('Processing attachment')
            self.process_attachment(api_doc, item.attachment_to_api_dict(), item.get('attachment'))

            return

        logging.debug('Processing web page')
        self.process_website(api_doc, item.doc_to_api_dict())

    @backoff.on_exception(wait_gen=backoff.expo,
                          exception=TooManyRequests,
                          jitter=None,
                          max_tries=settings.PUBLIC_S_API_MAX_RETRIES,
                          base=settings.PUBLIC_S_API_DELAY,
                          factor=2,
                          max_value=settings.PUBLIC_S_API_MAX_DELAY)
    def get_doc(self, url):

        url = clean_url(url)

        logging.debug(f'GET DOC: ?q=url:{url}')

        resp = requests.get(
            f'{self.api_endpoint}/api/records/?q=url.exact_match:"{url}"',
            headers=self.headers)

        if resp.status_code == HTTPStatus.TOO_MANY_REQUESTS:
            logging.debug('ERROR: Too many requests error occurred')
            raise TooManyRequests

        if resp.status_code != HTTPStatus.OK:
            # This means something went wrong.
            logging.error(f'GET ?q=url:{url} {resp.status_code}')
            return None

        hits = json.loads(resp.text)['hits']
        total = hits['total']
        if total > 0:  # Else means no hits, therefore origin doc doesn't exist
            if total > 1:
                logging.error(f'More than one hit when it should be unique. ?q=url:{url}')
            else:
                return hits['hits'][0]['metadata']
        else:
            logging.debug('No document was found (hits/total was 0)')
            return None

    def process_attachment(self, api_doc, new_doc, attachment):
        if api_doc:
            # patch
            logging.info('Attachment already exists')
            if settings.FORCE:
                logging.info('Force pushing attachment')
                resp = requests.put(
                    f'{self.api_endpoint}/api/record/{api_doc["control_number"]}/files/{new_doc["_data"]["name"]}',
                    headers=self.attachment_headers,
                    data=attachment
                )

                if resp.status_code != HTTPStatus.OK:
                    logging.error(f'Unable to POST attachment \n{resp.text}')
        else:
            # create
            logging.info(f'Adding new origin attachment {new_doc["_data"]["origin"]}')

            new_doc["$schema"] = f"{self.jsonschema_host}/schemas/webservices/file_v1.0.0.json"

            resp = requests.post(f'{self.api_endpoint}/api/records/', headers=self.headers, data=json.dumps(new_doc))

            if resp.status_code == HTTPStatus.TOO_MANY_REQUESTS:
                logging.debug('Too many requests error occurred')
                raise TooManyRequests

            if resp.status_code != HTTPStatus.CREATED:
                logging.error(f'Unable to POST document \n{resp.text}')

            resp_body = resp.json()['metadata']
            control = resp_body.get("control_number")
            resp = requests.put(
                f'{self.api_endpoint}/api/record/{control}/files/{new_doc["_data"]["name"]}',
                headers=self.attachment_headers,
                data=attachment
            )

            if resp.status_code != HTTPStatus.OK:
                logging.error(f'Unable to POST attachment \n{resp.text}')

    @backoff.on_exception(wait_gen=backoff.expo,
                          exception=TooManyRequests,
                          jitter=None,
                          max_tries=settings.PUBLIC_S_API_MAX_RETRIES,
                          base=settings.PUBLIC_S_API_DELAY,
                          factor=2,
                          max_value=settings.PUBLIC_S_API_MAX_DELAY)
    def process_website(self, api_doc, new_doc):

        # Document exists, therefore update it
        if api_doc:
            if new_doc['_data']['content'] != api_doc['_data']['content'] or \
                    new_doc['_data']['name'] != api_doc['_data']['name']:
                body = [
                    {
                        "op": "replace",
                        "path": "/_data/name",
                        "value": new_doc['_data']['name']
                    },
                    {
                        "op": "replace",
                        "path": "/_data/content",
                        "value": new_doc['_data']['content']
                    },
                    {
                        "op": "replace",
                        "path": "/analytics_relevance",
                        "value": new_doc['analytics_relevance']
                    },
                    {
                        "op": "replace",
                        "path": "/last_updated",
                        "value": new_doc['last_updated']
                    }
                ]
                return self.patch_doc(api_doc['control_number'], body)
            else:
                rid = api_doc['control_number']
                parent = api_doc['url']
                url = new_doc['url']
                logging.debug(f'Nothing to update for {rid} - {parent} - {url}')
        # Document doesn't exist
        else:
            logging.debug(f'Adding new origin document {new_doc["_data"]["origin"]}')

            resp = requests.post(f'{self.api_endpoint}/api/records/', headers=self.headers, data=json.dumps(new_doc))

            if resp.status_code == HTTPStatus.TOO_MANY_REQUESTS:
                logging.debug('Too many requets error occurred')
                raise TooManyRequests

            if resp.status_code != HTTPStatus.CREATED:
                logging.error(f'Unable to POST document \n{resp.text}')

    @backoff.on_exception(wait_gen=backoff.expo,
                          exception=TooManyRequests,
                          jitter=None,
                          max_tries=settings.PUBLIC_S_API_MAX_RETRIES,
                          base=settings.PUBLIC_S_API_DELAY,
                          factor=2,
                          max_value=settings.PUBLIC_S_API_MAX_DELAY)
    def patch_doc(self, rid, body):
        # Updated, replace extra content
        logging.debug(f'Query PATCH {self.api_endpoint}/api/record/{rid} \n{json.dumps(body)}')
        resp = requests.patch(f'{self.api_endpoint}/api/record/{rid}', headers=self.patch_headers, data=json.dumps(body))

        if resp.status_code == HTTPStatus.TOO_MANY_REQUESTS:
            logging.debug('Too many requests error occurred')
            raise TooManyRequests

        if resp.status_code != HTTPStatus.OK:
            aux_body = body
            body[0]['value'] = '...'
            logging.warning(f'Unable to PATCH document {rid} with jsonpatch\n{aux_body}')
            logging.debug(f'Unable to PATCH document {rid} with jsonpatch\n{body}')
