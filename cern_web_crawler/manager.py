#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018-2019, CERN
#
# This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".
# In applying this license, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as Intergovernmental Organization
# or submit itself to any jurisdiction.

import import_string
import multiprocessing as mp
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging
from twisted.internet import reactor, defer
import redis

from cern_web_crawler import settings
from cern_web_crawler.spiders.public import CERNPublic


class Manager:

    def __init__(self):
        self.sites_function = import_string(settings.PUBLIC_SITES_FUNCTION)

        # Configure Crawler process
        configure_logging()
        self.site_type = settings.PUBLIC_SITES_REST_TYPE
        self.spider, self.spider_name = self._get_spider_name()
        self.runner = CrawlerRunner(settings.build_settings(self.spider_name))

        # Redis
        self._redis_connect()

    def _get_spider_name(self):
        return {
            'Official': (CERNPublic, CERNPublic.name),
            'Personal': (CERNPublic, CERNPublic.name),
            'Test': (CERNPublic, CERNPublic.name)
        }[self.site_type]

    def _check_redis_connection(self):
        if not self.conn:
            self._redis_connect()

    def _redis_connect(self):
        try:
            self.conn = redis.StrictRedis(
                host=settings.REDIS_HOST,
                port=settings.REDIS_PORT,
                db=settings.REDIS_DB
            )
            self.conn.ping()
        except Exception as ex:
            exit('Failed to connect, terminating.')

    def _fill_redis(self):
        sources = self.sites_function()
        for site_type, urls in sources.items():
            for url in urls:
                if 'test' not in url and 'dev' not in url \
                        and not self.conn.get(url):
                    self.conn.set(name=url, value=site_type)

    @defer.inlineCallbacks
    def _crawl(self):
        key = self.conn.randomkey()
        while key is not None:
            key = key.decode('utf-8')
            redis_site_type = self.conn.get(name=key).decode('utf-8')
            if redis_site_type == self.site_type:
                yield self.runner.crawl(self.spider, start_urls=[key])
                self.conn.delete(key)
            key = self.conn.randomkey()
        reactor.stop()

    def crawl(self):

        def _iteration():
            self._check_redis_connection()
            self._fill_redis()
            self._crawl()
            reactor.run()

        process = mp.Process(target=_iteration)
        process.start()
        process.join()


manager = Manager()
manager.crawl()
