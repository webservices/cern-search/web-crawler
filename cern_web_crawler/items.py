#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2018, CERN
# This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".
# In applying this license, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as Intergovernmental Organization
# or submit itself to any jurisdiction.

"""
Define here the models for your scraped items
See documentation in:
https://doc.scrapy.org/en/latest/topics/items.html
"""

import scrapy


"""
CERN scrapped web site. This can be an origin (if origin is equals to url) or a followed url, and its represented by:
    - Name: Name of the webpage stracted by the metadata. Set to URL without protocol nor domain if it cannot be 
    extracted.
    - URL: URL of the website.
    - Website: Base URL of the site (e.g. website.cern.ch for website.cern.ch/xyz/xw).
    - Analytics relevance: Relevance of the site based on Matomo analytics.
    - Collection: Document collection to which the site belongs. Usually the same than the site type.
    - Origin: The origin url, meaning the website in which this link was found. Set to None if it is a starting url.
    - Content: Scrapped content of the websited. Full HTML.
    - Last updated: Last time of update.
    - attachment: Boolean value which specifies if the item is a Attachment/Followed Link or not
    - Instance: The CERN Search instance in which the documents should be indexed.
    
Take into account that a followed link website will also have its own "origin" item for consistency, since urls are only 
visited once.
"""


class CERNWebItem(scrapy.Item):
    # _data
    name = scrapy.Field()
    url = scrapy.Field()
    website = scrapy.Field()
    origin = scrapy.Field()
    content = scrapy.Field()
    authors = scrapy.Field()
    # _refiners
    promoted = scrapy.Field()
    promoted_keywords = scrapy.Field()
    collection = scrapy.Field()
    analytics_relevance = scrapy.Field()
    type_format = scrapy.Field()
    image_source = scrapy.Field()
    last_updated = scrapy.Field()
    # attachment
    attachment = scrapy.Field()

    def doc_to_api_dict(self):
        return {
            "_access": {
                "owner": ["CernSearch-Administrators@cern.ch"],
                "update": ["CernSearch-Administrators@cern.ch"],
                "delete": ["CernSearch-Administrators@cern.ch"]
            },
            "_data": {
                'name': self.get('name'),
                'site': self.get('website'),
                'origin': self.get('origin'),
                'content': self.get('content'),
                'authors': self.get('authors')
            },
            'url': self.get('url'),
            'promoted': self.get('promoted'),
            'promoted_keywords': self.get('promoted_keywords'),
            'collection': self.get('collection'),
            'image_source': self.get('image_source'),
            'analytics_relevance': self.get('analytics_relevance'),
            'last_updated': self.get('last_updated').strftime("%Y-%m-%dT%H:%M:%SZ")
        }

    def attachment_to_api_dict(self):
        return {
            "_access": {
                "owner": ["CernSearch-Administrators@cern.ch"],
                "update": ["CernSearch-Administrators@cern.ch"],
                "delete": ["CernSearch-Administrators@cern.ch"]
            },
            "_data": {
                'name': self.get('name'),
                'site': self.get('website'),
                'origin': self.get('origin'),
                'content': self.get('content'),
                'authors': self.get('authors')
            },
            'url': self.get('url'),
            'promoted': self.get('promoted'),
            'promoted_keywords': self.get('promoted_keywords'),
            'collection': self.get('collection'),
            'image_source': self.get('image_source'),
            'analytics_relevance': self.get('analytics_relevance'),
            'last_updated': self.get('last_updated').strftime("%Y-%m-%dT%H:%M:%SZ")
        }
