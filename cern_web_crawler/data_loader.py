#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  Copyright (C) 2018, CERN
#
#  This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".
# In applying this license, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as Intergovernmental Organization
# or submit itself to any jurisdiction.
import logging

import base64
import json

import requests

from cern_web_crawler import settings


def import_sites_from_file():
    filename = settings.PUBLIC_SITES_FILENAME
    with open(filename, "r") as sites:
        site_names = [x.strip() for x in sites.readlines()]
    sites.close()
    return {'Official': site_names}


def import_sites_from_rest():
    endpoint = settings.PUBLIC_SITES_REST_ENDPOINT
    credentials = settings.PUBLIC_SITES_REST_CREDENTIALS
    site_type = settings.PUBLIC_SITES_REST_TYPE

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json; charset=utf-8"
    }

    if credentials:
        headers['Authorization'] = f'Basic {base64.b64encode(credentials.encode("utf-8")).decode("utf-8")}'

    result = {}

    resp = requests.post(
        endpoint,
        data=json.dumps({'CategoryFilter': site_type}),
        headers=headers)
    if resp.status_code != 200:
        # This means something went wrong.
        logging.error(f'GET {endpoint} {resp.status_code}')
    else:
        result[site_type] = eval(resp.json().get('d'))
    return result
