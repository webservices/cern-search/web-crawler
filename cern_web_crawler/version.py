#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2018, CERN
# This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".
# In applying this license, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as Intergovernmental Organization
# or submit itself to any jurisdiction.


"""
Version information for CERN Search Web Crawler.
This file is imported by ``cern_web_crawler.__init__``,
and parsed by ``setup.py``.
"""

from __future__ import absolute_import, print_function

__version__ = '0.3.0a1'
