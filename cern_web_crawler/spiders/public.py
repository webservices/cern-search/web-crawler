#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018, CERN
#
# This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".
# In applying this license, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as Intergovernmental Organization
# or submit itself to any jurisdiction.

import datetime
import re

from bs4 import BeautifulSoup
from scrapy.http import Response
from scrapy.spiders import CrawlSpider, Rule
from urllib3.util import parse_url
from scrapy.linkextractors import LinkExtractor

from cern_web_crawler import settings
from cern_web_crawler.items import CERNWebItem

ALLOWED_EXTENSIONS = [
    # office suites: allowed
     'xls', 'xlsx', 'xlsm', 'ppt', 'pptx', 'pps', 'doc', 'docx', 'odt', 'ods', 'odg',
     'odp',  'pdf', 'txt', 'numbers', 'key', 'pages'
]

IGNORED_EXTENSIONS = [
    # images
    'mng', 'pct', 'bmp', 'gif', 'jpg', 'jpeg', 'png', 'pst', 'psp', 'tif',
    'tiff', 'ai', 'drw', 'dxf', 'eps', 'ps', 'svg',

    # audio
    'mp3', 'wma', 'ogg', 'wav', 'ra', 'aac', 'mid', 'au', 'aiff',

    # video
    '3gp', 'asf', 'asx', 'avi', 'mov', 'mp4', 'mpg', 'qt', 'rm', 'swf', 'wmv',
    'm4a', 'm4v', 'flv',

    # other
    'css', 'exe', 'bin', 'rss', 'xml', 'log', 'root', 'zip', 'rar', 'gz', 'rpm',
    'tex', 'nb', 'md5', 'dwf', 'sh', 'tsv', 'whl'
]


class CERNPublic(CrawlSpider):
    name = "cern.search.crawler.public"

    def __init__(self, *args, **kwargs):
        # init start urls
        super(CERNPublic, self).__init__(*args, **kwargs)

        base_url = parse_url(self.start_urls[0]).host
        # Offiste Middleware needs protocol but link extractor does not
        self.allowed_domains = [base_url]

        self.custom_settings = {
            'LOG_LEVEL': settings.PUBLIC_CRAWLER_LOGLEVEL
        }

        deny_extensions = settings.DENY_EXTENSIONS
        deny_extensions.update(IGNORED_EXTENSIONS)

        self.allow_extensions = settings.ALLOWED_EXTENSIONS
        self.allow_extensions.update(ALLOWED_EXTENSIONS)

        self.rules = (
            Rule(
                LinkExtractor(tags=('frame'), attrs=('src')),
                follow=True
            ),
            Rule(
                LinkExtractor(
                    allow_domains=self.allowed_domains,
                    deny=settings.DENY_LIST,
                    tags=('a', 'area'),
                    attrs=('href'),
                    deny_extensions=deny_extensions,
                    unique=True,
                    canonicalize=True,
                ),
                callback='parse_item',
                follow=True
            )
        )

        # init rules
        super(CERNPublic, self).__init__(*args, **kwargs)

    def parse_item(self, response: Response):
        # No need to check response.status == 200
        # since Scrapy only process successful requests
        if self.is_binary(response.headers.get('Content-Type', '').decode("utf-8")):
            file_extension = response.url.split('/')[-1].split('.')[-1]

            if file_extension.lower() not in self.allow_extensions:
                self.logger.warning(f"Filtered binary request {file_extension}: {response.url} ")

                return

            self.logger.debug(f"BINARY: {response.url} ")

            attachment_item = self.process_attachment(
                url=response.url,
                origin=response.meta.get('origin'),
                response=response
            )
            if attachment_item:
                return attachment_item

        if settings.CRAWL_WEB:
            # Get based website item from request response
            web_item = self.process_website(response)

            # Every FL should be yield also as individual page
            # to produce a result by itself if relevant
            self.logger.debug(f"Item OT: url {web_item.get('url')} and origin {web_item.get('origin')}")

            return web_item

    def is_binary(self, content_type):
        # Content-Type might come with charset
        clean_ct = content_type.split(';')[0]
        return {clean_ct}.isdisjoint(self.settings.get('TEXT_CONTENT_TYPES'))

    def process_attachment(self, url, origin, response):
        base_url, full_url = self.process_url(url)
        origin = response.meta.get('origin') if response.meta.get('origin') else url

        return CERNWebItem(
            name=response.url.split('/')[-1],
            url=full_url,
            website=base_url,
            analytics_relevance=-1,
            promoted=False,
            promoted_keywords=[],
            origin=origin,
            last_updated=datetime.datetime.utcnow(),
            attachment=response.body,
            image_source='',
            type_format=''
        )

    @staticmethod
    def process_url(url):
        components = parse_url(url)
        base = components.host
        path = components.path
        return (base, base + path) if path else (base, base)

    @staticmethod
    def process_content(soup):
        # remove all script and style elements
        for script in soup(["script", "style"]):
            script.extract()  # rip it out

        # get text
        # Double encode/decode with the 'ignore' option to remove non utf-8 characters
        text = soup.get_text().encode('utf-8', 'ignore').decode('utf-8')
        # break into lines and remove leading and trailing space on each
        lines = (line.strip() for line in text.splitlines())
        # break multi-headlines into a line each
        chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
        # drop blank lines
        text = '\n'.join(chunk for chunk in chunks if chunk)
        # remove escaped characters
        text = re.sub('\s+', ' ', text)
        return text

    @staticmethod
    def process_name(title, base_url):
        if title:
            return title.string
        else:
            parts = base_url.split('.')[0].split('-')
            return ' '.join(map(str.capitalize, parts))

    def process_collection(self, base_url):
        return 'Documentation' if '.docs' in base_url else 'Web page'

    def process_website(self, response):
        soup = BeautifulSoup(response.text, "lxml")
        base_url, url = self.process_url(response.url)
        name = self.process_name(soup.title, base_url)
        origin = response.meta.get('origin') if response.meta.get('origin') else url
        collection = self.process_collection(base_url)
        type_format = f'{collection} site'

        # Return Web item and the list of links to follow
        return CERNWebItem(
            name=name,
            url=url,
            website=base_url,
            origin=origin,
            content=self.process_content(soup),
            authors=['Anonymous'],
            promoted=False,
            promoted_keywords=[],
            collection=collection,
            type_format=type_format,
            image_source='',
            analytics_relevance=-1,
            last_updated=datetime.datetime.utcnow()
        )
