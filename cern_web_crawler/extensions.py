#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018-2019, CERN
#
# This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".
# In applying this license, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as Intergovernmental Organization
# or submit itself to any jurisdiction.
import logging

import sentry_sdk
from scrapy.exceptions import NotConfigured
from sentry_sdk.integrations.logging import LoggingIntegration


class SentryLogging(object):
    """
    Send exceptions and errors to Sentry.
    """

    @classmethod
    def from_crawler(cls, crawler):
        sentry_dsn = crawler.settings.get('SENTRY_DSN', None)

        if sentry_dsn is None:
            raise NotConfigured
        # instantiate the extension object
        ext = cls()

        # All of this is already happening by default!
        sentry_logging = LoggingIntegration(
            level=logging.INFO,  # Capture as breadcrumbs
            event_level=crawler.settings.get('SENTRY_LOGLEVEL')  # Send errors as events
        )

        # instantiate
        sentry_sdk.init(dsn=sentry_dsn, integrations=[sentry_logging])
        # return the extension object
        return ext
