# Copyright (C) 2018-2019, CERN
#
# This software is distributed under the terms of the GNU General Public
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".
# In applying this license, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as Intergovernmental Organization
# or submit itself to any jurisdiction.

# Use CentOS7:
FROM centos/python-36-centos7

ADD . /scrapy
WORKDIR /scrapy

USER root

# Install pre-requisites
RUN yum update -y && \
    yum install -y epel-release && \
    yum install -y redis && \
    yum install telnet telnet-server -y

RUN pip install --upgrade pip && pip install -e .

USER default

CMD ["python cern_web_crawler/manager.py"]